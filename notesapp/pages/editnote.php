<?php
session_start();
include('authen.php');
include('secure.php');

$user=isset($_SESSION['uid'])?$_SESSION['uid']:'';
$myPass=isset($_SESSION['password'])?$_SESSION['password']:'';
$myRole=isset($_SESSION['role'])?$_SESSION['role']:'';
$msg="";
$cla = "alert alert-success";
$noteid = $_GET['edit'];
$getnote = mysqli_fetch_array(mysqli_query($conn, "select * from notes where noteid='$noteid'")) or die(mysqli_error($conn));
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NoteApp:: Modify Note</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
   <?php include("header.php"); ?>
            <!-- /.navbar-header -->

            <?php include("header_menu.php") ?>
            <!-- /.navbar-top-links -->

            <?php include("menu.php") ?>
            <!-- /.navbar-static-side -->
        </nav>
		<?php
		$dm = date("d/m/Y H:m:s");
		$d = date("dmYhms");
		if(array_key_exists('ok',$_POST)){
			$title = stripslashes($_POST["title"]);
			$note = stripslashes($_POST["note"]);
			mysqli_query($conn, "update notes set title = '$title',note='$note',dm='$dm' where noteid='$noteid'")or die(mysqli_error());
			header("location:notes.php");
			$msg="Note Updated Successfully!";
		}
		?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">NoteApp: Note Details</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          Modify Note
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                           <div class="row">
                                <div class="col-lg-6">
                                    <p class="<?php echo $cla ?>"><?php echo $msg ?></p>
                                    <form role="form" name="form1" method="post" action="">
                                        
                                        <div class="form-group">
                                            <label>Note Title</label>
                                            <input class="form-control" name="title" placeholder="Enter Title" value="<?php echo $getnote['title']?>" required>
                                        </div>
                                    <div class="form-group">
                                            <label>Type Note </label>
                                            <textarea class="form-control" rows="5" cols="45" name="note" placeholder="Note content goes here..." required><?php echo $getnote['note']?></textarea>
                                        </div>    
                                     
                                            <input type="submit" name="ok" class="btn btn-default" value="Save Changes">
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                           
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                              
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.table-responsive -->
                         
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <link href="datepicker/datepicker.css" rel="stylesheet" media="screen">
			<script src="datepicker/bootstrap-datepicker.js"></script>
						<script>
						$(function() {
							$(".datepicker").datepicker();
							$(".uniform_on").uniform();
							$(".chzn-select").chosen();
							$('#rootwizard .finish').click(function() {
								alert('Finished!, Starting over!');
								$('#rootwizard').find("a[href*='tab1']").trigger('click');
							});
						});
						</script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {

        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>
