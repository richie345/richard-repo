<?php
session_start();

session_unset($_SESSION["is_loged"]);
session_unset($_SESSION["pass"]);

session_destroy();
header("Location:index.php");
?>