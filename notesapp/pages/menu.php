<?php
$myRole=isset($_SESSION['role'])?$_SESSION['role']:'';
if($myRole=='admin'){
?>
<div class="navbar-default sidebar" role="navigation" style="max-height:500px; overflow-y:scroll">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                             <!--   <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span> -->
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="home.php"><i class="fa fa-dashboard fa-fw"></i> HOME</a>
                        </li>
                        
                      
                        <li>
                            <a href="notes.php"><i class="fa fa-file-o fa-fw"></i> Notes</a>
                        </li>
                        
                        <li>
                            <a href="out.php"><i class="fa fa-power-off fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <?php
}elseif($myRole=='user'){
?>
<div class="navbar-default sidebar" role="navigation" style="max-height:500px; overflow-y:scroll">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                             <!--   <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span> -->
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="home.php"><i class="fa fa-dashboard fa-fw"></i> HOME</a>
                        </li>
                        
                        <li>
                            <a href="out.php"><i class="fa fa-power-off fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <?php
}
			?>